r = Repository::current_repository
r.name= "cyloncore-core"

class OpenSSLRecipe < Recipe
    def initialize(package, platform, build_mode)
      super
    end
    def configured?
      return super
      return (super and  File.exists?("#{self.build_dir}/Makefile"))
    end
    def configure
      super
      if(self.platform.name == "android")
        run_command("cd #{self.build_dir}; #{self.package.checkout_directory}/Configure #{get_target()} no-shared --openssldir=#{self.platform.install_dir} --prefix=#{self.platform.install_dir}", get_env())
      elsif(self.platform.name == "ios" or self.platform.name == "ios-simulator")
        run_command("cd #{self.build_dir}; #{self.package.checkout_directory}/Configure ios64-cross no-shared no-dso no-hw no-engine --prefix=#{self.platform.install_dir}", get_env())
      else
        puts "\n\n**** Configuration failure ****\n\n\n".red
        puts "OpenSSL building is not supported on platform '#{self.platform.name}'."
        exit -1
      end
    end
    def build
      super
      if(self.platform.name == "ios")
        run_command("cd #{self.build_dir}; make -j#{Settings.cmake[:parallel_jobs]} CC='clang -fembed-bitcode'", get_env())
      else
        run_command("cd #{self.build_dir}; make -j#{Settings.cmake[:parallel_jobs]}", get_env())
      end
    end
  def install()
    super
    if(self.platform.name == "android")
      run_command("cd #{self.build_dir}; make -j#{Settings.cmake[:parallel_jobs]} install; make -j#{Settings.cmake[:parallel_jobs]} uninstall_docs", get_env())
    else
      run_command("cd #{self.build_dir}; make -j#{Settings.cmake[:parallel_jobs]} CC='clang -fembed-bitcode' install", get_env())
    end
  end
private
  def get_env()
    env = {}
    if(self.platform.name == "android32" or self.platform.name == "android64")
      env["ANDROID_NDK_HOME"] = ENV['ANDROID_NDK_ROOT']
      env["PATH"] = "#{ENV['ANDROID_NDK_ROOT']}/toolchains/llvm/prebuilt/linux-x86_64/bin:#{ENV["PATH"]}"
      env["CC"] = "clang"
    elsif(self.platform.name == "ios" or self.platform.name == "ios-simulator")
      env["CC"] = "clang"
      env["CROSS_TOP"] = "/Applications/Xcode.app/Contents/Developer/Platforms/iPhoneOS.platform/Developer"
      env["CROSS_SDK"] = "iPhoneOS.sdk"
      env["PATH"] = "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin"
    else
      puts "\n\n**** Configuration failure ****\n\n\n".red
      puts "OpenSSL building is not supported on platform '#{self.platform.name}'."
      exit -1
    end
    return env
  end
  def get_target()
    if self.platform.name == "android32"
      return "android-arm"
    elsif self.platform.name == "android64"
      return "android-arm64"
    elsif self.platform.name == "ios"
      return "ios64-cross"
    end
    print("Unsupported platform for building OpenSSL: #{self.platform.name}")
    exit -1
  end
end

class FinalCutRecipe < Recipe
  def initialize(package, platform, build_mode)
    super
  end
  def configured?
    return (super and  File.exists?("#{self.build_dir}/Makefile"))
  end
    def configure
      unless File.directory?(self.build_dir)
        super
        run_command("cd #{self.build_dir}; git clone #{self.package.checkout_directory} .", get_env())
      end
      run_command("cd #{self.build_dir}; autoreconf --install --force", get_env())
      extra_config_opt = ""
      if(build_mode == BuildMode::DEBUG)
        extra_config_opt += ' CPPFLAGS="-DDEBUG" CXXFLAGS="-g -O0 -DDEBUG -W -Wall -pedantic" '
      end
      run_command("cd #{self.build_dir}; ./configure #{extra_config_opt} --prefix=#{self.platform.install_dir}", get_env())
    end
    def build
      super
      run_command("cd #{self.build_dir}; make -j#{Settings.cmake[:parallel_jobs]}", get_env())
    end
  def install()
    super
    run_command("cd #{self.build_dir}; make install", get_env())
  end
private
  def get_env()
    return {}
  end
end

# For 32bits:
# ANDROID_NDK_HOME=/home/cyrille/Frameworks/android/ndk/android-ndk-r18b/ PATH=/home/cyrille/Frameworks/android/ndk/android-ndk-r18b/toolchains/llvm/prebuilt/linux-x86_64/bin:$PATH CC=clang ../Configure android-arm shared
# ANDROID_NDK_HOME=/home/cyrille/Frameworks/android/ndk/android-ndk-r18b/ PATH=/home/cyrille/Frameworks/android/ndk/android-ndk-r18b/toolchains/llvm/prebuilt/linux-x86_64/bin:$PATH CC=clang make

# For 64bits: replace android-arm with android-arm64

# third party
create_package('qt-natvis', 'Qt Natvis for VS Code', '', [], [], "https://github.com/aambrosano/qt-natvis.git", Git, Recipe)
create_package('llama.cpp', '', 'Machine Learning', [], [], 'https://github.com/ggerganov/llama.cpp', Git, Recipes::CmakeRecipe)
create_package('openssl', 'OpenSSL', 'Misc', [], [], 'https://www.openssl.org/source/openssl-1.1.1c.tar.gz', Tar, OpenSSLRecipe).add_option("platform.host.enabled", false)
create_package('Sqlite3', 'Sqlite3', 'Database', [], [], 'git@bitbucket.org:pixelab/sqlite3-cmake.git', Git, Recipes::CmakeRecipe)
create_package('FinalCut', 'FinalCut', 'GUI', [], [], 'https://github.com/gansm/finalcut.git', Git, FinalCutRecipe).add_option("command.build.explicit", true)

# computer vision
create_package('libsegments', '', 'Misc', [], [], 'https://gitlab.com/cyloncore/libsegments', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/libsegments.git')

# clog/cerr
create_package('cext', '', 'Misc', [], [], 'https://gitlab.com/cyloncore/cext', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/cext.git').add_option('vc.switch_branch', ['master', 'stable'])

# Generators
create_package('ag', 'API Generator', 'CylonCore', ['ptc'], [], 'https://gitlab.com/cyloncore/ag', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/ag.git').add_option('vc.switch_branch', ['master', 'stable'])
create_package('ptc', 'ptc template compiler', '', [], [], "https://gitlab.com/cyloncore/ptc.git", Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/ptc.git').add_option('vc.switch_branch', ['master', 'stable']).add_option("platform.android.enabled", false).add_option("platform.ios.enabled", false)
create_package('parc', 'parc active record compiler', '', ['ptc'], [], "https://gitlab.com/cyloncore/parc", Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/parc.git').add_option('vc.switch_branch', ['master', 'stable']).add_option("platform.android.enabled", false).add_option("platform.ios.enabled", false)

# GQLite
create_package('gqlite', 'GQLite', 'Misc', [], [], 'https://gitlab.com/cyloncore/gqlite.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/gqlite.git')
create_package('gqlclib', 'gqlclib', 'Misc', [], [], 'https://gitlab.com/cyloncore/gqlclib.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/gqlclib.git')
create_package('gqlcl', 'gqlcl', 'Misc', [], [], 'https://gitlab.com/cyloncore/gqlcl.git', Git, Recipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/gqlcl.git')
create_package('gqlitebrowser', 'GQLite Browser', 'Misc', [], [], 'https://gitlab.com/cyloncore/gqlitebrowser.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/gqlitebrowser.git')

# chatTB
create_package('chatTB', 'Chat ToolBox', 'Misc', [], ['pralin'], 'https://gitlab.com/cyloncore/chattb.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/chattb.git')

# Cyqlops
create_package('Cyqlops', 'Cyqlops', 'CylonCore', [], ['FinalCut', 'Sqlite3', 'llama.cpp'], 'https://gitlab.com/cyloncore/cyqlops.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/cyqlops.git')

# tasks_machine
create_package('tasks_machine', 'tasks_machine', 'CylonCore', [], [], 'https://gitlab.com/cyloncore/tasks_machine.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/tasks_machine.git').add_option('vc.switch_branch', ['master', 'stable'])

# GIS Related
create_package('geos', 'GEOS -- Geometry Engine, Open Source', 'Misc', [], [], 'https://github.com/libgeos/geos.git', Git, Recipes::CmakeRecipe).add_option("command.build.explicit", true).add_option('vc.tag', '3.9.0').add_option("recipe.cmake.CMAKE_INSTALL_RPATH_USE_LINK_PATH", "TRUE").add_option("recipe.cmake.CMAKE_INSTALL_RPATH", "#{ENV['LRS_PKG_ROOT']}/inst/lib")
create_package('euclid', 'Generic Interface to Geometric Libraries', 'Misc', [], ['geos', 'cext'], 'https://gitlab.com/cyloncore/euclid', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/euclid.git').add_option('vc.switch_branch', ['master', 'stable'])
create_package('Cartography', '', 'CylonCore', ['euclid'], ['Cyqlops', 'cext', 'pybind11-qt'], 'https://gitlab.com/cyloncore/cartography.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/cartography.git').add_option('vc.switch_branch', ['master', 'stable'])
create_package('cartography-rs', '', 'CylonCore', [], [], 'https://gitlab.com/cyloncore/cartography-rs.git', Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/cartography-rs.git')

# Other
create_package('pybind11-qt', 'Helpers to write Qt bindings with pybind11', '', [], [], "https://gitlab.com/cyloncore/pybind11-qt", Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/pybind11-qt.git')

create_package('Cauchy', 'Cauchy Matlab to Eigen compiler', '', [], [], "https://gitlab.com/cyloncore/cauchy", Git, Recipes::CmakeRecipe).add_option('vc.push_url', 'git@gitlab.com:cyloncore/cauchy.git')

create_package('pRpc', 'pRpc Remote Procedure Call', 'Misc', ['ptc'], [], "git@gitlab.com:cyloncore/prpc.git", Git, Recipes::CmakeRecipe).add_option("platform.ios.cmake.PTC_PROGRAM", "#{ENV['LRS_PKG_ROOT']}/inst/bin/ptc")
create_package('shiva', 'shiva Lang Library', '', [], [], "git@gitlab.com:shivalang/shiva.git", Git, Recipes::CmakeRecipe)
create_package('shiva_kernels', 'Collection of shiva kernels', '', [], [], "git@gitlab.com:shivalang/shiva_kernels.git", Git, Recipes::CmakeRecipe)
create_package('ImaGP', '', '', [], ['shiva', 'shiva_kernels'], "git@gitlab.com:pictoscape/imagp.git", Git, Recipes::CmakeRecipe)
create_package('ImaCP', '', '', ['ImaGP', 'shiva_kernels', 'shiva'], [], "git@gitlab.com:pictoscape/imacp.git", Git, Recipes::CmakeRecipe)
create_package('ImaIO', 'ImaIO', 'PixelAB', [], [], 'git@gitlab.com:pictoscape/imaio.git', Git, Recipes::CmakeRecipe)
create_package('ImaMD', 'ImaMD', 'PixelAB', [], [], 'git@gitlab.com:pictoscape/imamd.git', Git, Recipes::CmakeRecipe)
create_package('ImaDB', 'ImaDB', 'PixelAB', ['ImaIO', 'ImaMD', 'pRpc', 'parc', 'QtSqlite3'], [], 'git@gitlab.com:pictoscape/imadb.git', Git, Recipes::CmakeRecipe)
create_package('ImaCC', 'ImaCC', 'PixelAB', [], [], 'git@gitlab.com:pictoscape/imacc.git', Git, Recipes::CmakeRecipe)
create_package('DJIMobileWebClient', 'Library to communicate with the DjiMobileWebService from a computer', 'Misc', [], [], 'git@gitlab.com:pixelab.se/djimobilewebclient.git', Git, Recipes::CmakeRecipe)
create_package('Curvature', '', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/curvature.git', Git, Recipes::CmakeRecipe)
create_package('ShapesML', '', 'CylonCore', ['Cyqlops'], [], 'git@gitlab.com:cyloncore/shapesml.git', Git, Recipes::CmakeRecipe)
create_package('icvm', 'Instructions in C for Virtual Machine', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/icvm.git', Git, Recipes::CmakeRecipe)

# Rust
create_package('brevy_ratepace', 'Pace control for bevy.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/bevy_ratepace.git', Git, Recipes::CargoRecipe)
create_package('brisk', 'Declarative Engine for Rust.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/brisk.git', Git, Recipes::CargoRecipe)
create_package('cartography-rs', 'Map rendering for Rust.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/cartography-rs.git', Git, Recipes::CargoRecipe)
create_package('ccutils', 'Utils for CylonCore.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/ccutils.git', Git, Recipes::CargoRecipe)
	create_package('compute-it', 'Computation Graph Library.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/compute-it.git', Git, Recipes::CargoRecipe)
create_package('egui_node_editor', 'Declarative Engine for Rust.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/egui_node_editor.git', Git, Recipes::CargoRecipe)
create_package('yaaral', 'Async Runtime Selector for Cylon Core projects.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/yaaral.git', Git, Recipes::CargoRecipe)

create_package('mqtt-channel', 'MQTT Channels.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/mqtt-channel.git', Git, Recipes::CargoRecipe)
create_package('mqtt-service', 'MQTT Services.', 'CylonCore', [], [], 'git@gitlab.com:cyloncore/mqtt-service.git', Git, Recipes::CargoRecipe)
